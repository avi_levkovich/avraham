<?php
/**
 * Created by PhpStorm.
 * User: Avi Levkovich (http://www.levkovich.co.il)
 * Date: 28/11/2017
 * Time: 20:48
 */

require_once '../init.php';

use admin\generator;
use admin\playlist_builder;

use PHPUnit\Framework\TestCase;

class generatorTest extends TestCase {

	protected function admin() {
		global $current_user;
		$current_user = new WP_User( 1 );
	}

	protected function get_requests( $file ) {
		$requests = array_map( function ( $file ) {
			return file_get_contents( $file );
		}, glob( dirname( __FILE__ ) . '/' . $file . '/*.txt' ) );

		return $requests;
	}

	public function testSubmit() {
		$this->admin();

		foreach ( $this->get_requests( 'submit-good' ) as $request ) {
			$generate      = new generator( $request );
			$result        = $generate->add_playlist();
			$url           = json_decode( $result );
			$playlist_post = get_post( $generate->getPlaylist() );
			$this->assertEquals( $url->url, get_edit_post_link( $playlist_post->ID ) );
		}
	}

	public function testSubmitBad() {
		$this->admin();

		foreach ( $this->get_requests( 'submit-bad' ) as $request ) {
			$generate      = new generator( $request );
			$result        = $generate->add_playlist();
			$url           = json_decode( $result );
			$check=isset( $url->error ) && isset($url->error->errors) && isset($url->error->errors->{'Not a valid json'});
			$this->assertTrue( $check );
		}
	}

	public function testBuildPlaylistWrap() {
		$this->admin();

		foreach ( $this->get_requests( 'submit-good' ) as $request ) {
			$generate = new generator( $request );
			$post     = $generate->get_post();
			parse_str( $post, $output );
			$this->assertTrue( isset( $output['package'] ) );
			$result        = $generate->test( 'build_playlist', $output );
			$url           = json_decode( $result );
			$playlist_post = get_post( $generate->getPlaylist() );
			$this->assertEquals( $url->url, get_edit_post_link( $playlist_post->ID ) );
		}
	}

	public function testBuildPlaylistWrapBad() {
		$this->admin();

		foreach ( $this->get_requests( 'submit-bad' ) as $request ) {
			$generate = new generator( $request );
			$post     = $generate->get_post();
			parse_str( $post, $output );
			$this->assertTrue( isset( $output['package'] ) );
			$result = $generate->test( 'build_playlist', $output );
			$url    = json_decode( $result );
			$check=isset( $url->error ) && isset($url->error->errors) && isset($url->error->errors->{'Not a valid json'});
			$this->assertTrue( $check );
		}
	}

	public function testBuildPlaylist() {
		foreach ( $this->get_requests( 'submit-good' ) as $request ) {
			$generate = new generator( $request );
			$post     = $generate->get_post();
			parse_str( $post, $output );
			$this->assertTrue( isset( $output['package'] ) );
			$generate->test( 'setPackage', json_decode( $output['package'] ) );
			$playlist = new playlist_builder( $generate->getPackage() );
			$result   = $playlist->build();
			$this->assertTrue( ! ! $result->getId() );
			$playlist_post = get_post( $result->getId() );
			$ids           = explode( ',', $playlist_post->post_excerpt );
			foreach ( $ids as $id ) {
				$this->assertTrue( $playlist->in_items( $id ) );
			}
		}
	}

	public function testBuildPlaylistBad() {
		$this->admin();

		foreach ( $this->get_requests( 'submit-bad' ) as $request ) {
			$generate = new generator( $request );
			$post     = $generate->get_post();
			parse_str( $post, $output );
			$this->assertTrue( isset( $output['package'] ) );
			$decoded = json_decode( $output['package'] );
			$this->assertTrue( ! $decoded );
		}
	}

	protected function setUp() {
		add_filter( 'determine_current_user', function () {
			return 1;
		} );
	}


}

<?php
/**
 * Created by PhpStorm.
 * User: Avi Levkovich
 * Date: 18/04/2017
 * Time: 19:32
 */

if ( isset( $_SERVER['REQUEST_URI'] ) ) {
	$uri = $_SERVER['REQUEST_URI'];
} else {
	$uri = $_SERVER['SCRIPT_NAME'];
}

define( 'PHPUNIT', true );

$path = explode( 'wp-content', $uri );

if ( isset( $_SERVER['IDE_PHPUNIT_PHPUNIT_PHAR'] ) ) {
	$path = array( str_replace( 'phpunit.phar', '', $_SERVER['IDE_PHPUNIT_PHPUNIT_PHAR'] ) );
}

$wpload = $_SERVER['DOCUMENT_ROOT'] . $path[0] . 'wp-load.php';

if ( ! file_exists( $wpload ) ) {
	$path   = explode( 'vendor', $uri );
	$wpload = $_SERVER['DOCUMENT_ROOT'] . $path[0] . 'wp-load.php';
}

require_once $wpload;
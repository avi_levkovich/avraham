<?php
/**
 * Created by PhpStorm.
 * User: Avi Levkovich (http://www.levkovich.co.il)
 * Date: 25/01/2018
 * Time: 00:17
 */

return;
add_filter( 'show_list_tables', function ( $array, $tables ) {
	$show = array(
		'wp5k_archive',
		'wp5k_archive_meta'
	);

	$tables = array_intersect( $show, $tables );

	return $tables;
}, 10, 2 );

add_filter( 'orm_edit_editor', function ( $return, $table, $field ) {
	global $wpdb;
	if ( $table == $wpdb->prefix . 'archive' ) {
		switch ( $field ) {
			case 'post_content':
			case 'post_excerpt':
				return true;
		}
	}

	return $return;
}, 10, 3 );
<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 23/09/2017
 * Time: 02:35
 */

add_action( 'slim_mapping', function ( $slim ) {
	$slim->get( '/slim/api/:page', function ( $page ) {
		$class = '\api\\' . $page;
		if ( class_exists( $class ) ) {
			$instance = new $class;
			$instance->json();
		}
	} );
} );

add_action( 'slim_mapping', function ( $slim ) {
	$slim->get( '/slim/api/subsonic/:action', function ( $action ) use ( $slim ) {
		$page  = 'subsonic';
		$class = '\api\\' . $page;
		if ( class_exists( $class ) ) {
			unset( $_GET['XDEBUG_SESSION_START'] );
			$args     = $_GET;
			$instance = new $class( $action, $args );
			$instance->json();
		}
	} );
} );
<?php
/**
 * Created by PhpStorm.
 * User: Avi Levkovich (http://www.levkovich.co.il)
 * Date: 26/11/2017
 * Time: 23:44
 */

if ( isset( $_GET['design'] ) && class_exists( sprintf( '\design\%s', $_GET['design'] ) ) ) {
	add_filter( 'site_design', function () {
		return $_GET['design'];
	} );
}
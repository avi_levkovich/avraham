<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 29/10/2017
 * Time: 00:57
 */

add_filter( 'of_options', function($options){
	$options[] = array(
		'name' => __( 'Subsonic', 'avraham' ),
		'type' => 'heading'
	);

	$options[] = array(
		'name' => 'username',
		'id'   => 'subsonic_username',
		'class' => 'mini',
		'type' => 'text'
	);

	$options[] = array(
		'name' => 'password',
		'id'   => 'subsonic_password',
		'class' => 'mini',
		'type' => 'text'
	);

	$options[] = array(
		'name' => 'server',
		'id'   => 'subsonic_server',
		'type' => 'text'
	);

	return $options;
}, 12 );
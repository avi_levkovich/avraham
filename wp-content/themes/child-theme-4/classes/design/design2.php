<?php
/**
 * Created by PhpStorm.
 * User: Avi Levkovich (http://www.levkovich.co.il)
 * Date: 26/11/2017
 * Time: 23:38
 */

namespace design;


class design2 extends abstract_design {
	function header() {
		get_header();
	}

	function footer() {
		get_footer();
	}

	function body() {
		echo '2';
	}

	public function css() {
		// TODO: Implement css() method.
	}

	public function js() {
		// TODO: Implement js() method.
	}

}
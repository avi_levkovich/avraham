<?php
/**
 * Created by PhpStorm.
 * User: Avi Levkovich (http://www.levkovich.co.il)
 * Date: 26/11/2017
 * Time: 23:38
 */

namespace design;


class default_design extends abstract_design {
	function header() {
		get_header();
	}

	function footer() {
		get_footer();
	}

	function body() {
		if(is_single()){
			global $post;
			echo '<pre>';
			print_r($post);
			echo '<pre>';
		}

		if(is_404()){
			echo '404';
		}
		dynamic_sidebar( 'homepage' );
	}

	static public function css() {
		return array(
			//locate_template( '/views/css/style4.css' )
			get_stylesheet_directory_uri() . '/views/css/style4.css'
		);
	}

	static public function js() {
		return array(
			get_stylesheet_directory_uri() . '/views/js/script4.js'
		);
	}

}
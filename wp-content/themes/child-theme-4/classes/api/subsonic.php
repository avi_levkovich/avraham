<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 28/10/2017
 * Time: 23:54
 */

namespace api;


class subsonic extends abstract_api {
	public $action;
	public $args;
	public $salt;
	protected $url;
	protected $username;
	protected $endpoint;
	protected $password;
	protected $response;
	protected $version = '1.13.0';
	protected $client = 'myapp';
	public $format = 'json';

	// http://avi111.subsonic.org/rest/getIndexes.view?u=avi111&t=764520672f3334b7444c56c3f9d88c5c&s=c19b2d&v=1.13.0&c=myapp
	public function __construct( $action, $args = '' ) {
		$this->action   = $action;
		$this->args     = $args;
		$this->username = of_get_option( 'subsonic_username' );
		$this->password = of_get_option( 'subsonic_password' );
		$this->endpoint = of_get_option( 'subsonic_server' ) . '/rest/';
		$this->salt     = substr( md5( microtime() ), rand( 0, 26 ), 6 );

	}

	protected function token_and_salt() {
		$token = md5( "{$this->password}{$this->salt}" );
		$salt  = $this->salt;

		return "&t={$token}&s={$salt}";
	}

	protected function get_class() {
		$action = \util\util::kebab_case_from_camel_case( $this->action );
		$class  = "\subsonic\\{$action}";

		return $class;
	}

	protected function concat_action_and_args() {
		$class = $this->get_class();
		if ( class_exists( $class ) ) {
			$array = ( new $class( $this->args ) )->execute();
			array_walk( $array, function ( $value, $key ) use ( &$array ) {
				$array[ $key ] = implode( '=', array( $key, $value ) );
			} );
			$array[] = '';
			$args    = implode( '&', $array );
		} else {
			$args = '';
		}

		return "{$this->action}.view?{$args}";
	}

	public function prepare() {
		$this->url = sprintf( '%s%su=%s%s&v=%s&c=%s&f=%s', $this->endpoint, $this->concat_action_and_args(), $this->username, $this->token_and_salt(), $this->version, $this->client, $this->format );

		return $this;
	}

	public function send() {
		$response = ( wp_remote_get( $this->url ) );
		$return   = wp_remote_retrieve_body( $response );

		return $return;
	}

	protected function output() {
		$this->response = $this->prepare()->send();

		return $this->response;
	}

	public function json($echo=true) {
		$class  = $this->get_class();
		$output = $this->output();
		if ( class_exists( $class )) {
			$output = ( new $class( $this->args ) )->output( $output );
		}
		if($echo) {
			echo $output;
		} else {
			return $output;
		}
	}


}
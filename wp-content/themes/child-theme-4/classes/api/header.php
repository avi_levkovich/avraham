<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 23/09/2017
 * Time: 04:32
 */

namespace api;


class header extends abstract_api {

	/**
	 * header constructor.
	 */

	protected $items;

	public function __construct() {
		$items = \util\util::getMenu( 'header' );

		if ( $items ) {
			foreach ( $items as $key => $value ) {
				$vars = get_object_vars( $value );
				foreach ( array_keys( $vars ) as $var ) {
					if ( ! in_array( $var, array(
						'title',
						'url',
						'menu_item_parent',
						'ID'

					) ) ) {
						unset( $value->{$var} );
					}

					if ( $var == 'url' ) {
						$value->{$var} = trim( str_replace( site_url(), '', $value->{$var} ), '/' );
					}
				}
				if ( $value->menu_item_parent ) {
					if ( ! isset( $array[ $value->menu_item_parent ] ) ) {
						$array[ $value->menu_item_parent ]['children'] = array();
					}

					$array[ $value->menu_item_parent ]['children'][] = $value;
				} else {
					$array[ $value->ID ]['parent'] = $value;
				}

				$this->items = $array;
			}
		} else {
			$this->items=array();
		}
	}

	protected function output() {
		return $this->items;
	}
}
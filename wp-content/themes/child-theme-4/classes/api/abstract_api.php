<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 23/09/2017
 * Time: 04:37
 */

namespace api;


abstract class abstract_api {
	abstract protected function output();

	public function json(){
		echo json_encode($this->output());
	}
}
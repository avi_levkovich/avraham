<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 29/10/2017
 * Time: 00:21
 */

namespace subsonic;

class download extends abstract_api {
	public function execute() {
		return $this->args;
	}

	public function output( $output ) {
		$song = new \api\subsonic( 'getSong', $this->args );
		$info = ( json_decode( $song->json( false ) ) )->{'subsonic-response'};
		if ( isset( $info->song ) ) {
			$details  = $info->song;
			$filename = sprintf( '%s.%s', $details->title, $details->suffix );
		} else {
			$filename = 'file.mp3';
		}
		$handle = fopen( $filename, "w" );
		fwrite( $handle, $output );
		fclose( $handle );

		header( 'Content-Type: application/octet-stream' );
		header( 'Content-Disposition: attachment; filename=' . basename( $filename ) );
		header( 'Expires: 0' );
		header( 'Cache-Control: must-revalidate' );
		header( 'Pragma: public' );
		header( 'Content-Length: ' . filesize( $filename ) );
		readfile( $filename );
		unlink($filename);
		exit;
	}
}
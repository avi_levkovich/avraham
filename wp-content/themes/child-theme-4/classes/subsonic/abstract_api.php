<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 23/09/2017
 * Time: 04:37
 */

namespace subsonic;


abstract class abstract_api {
	protected $args;

	/**
	 * abstract_api constructor.
	 */
	public function __construct( $args ) {
		$this->args = $args;
	}

	abstract public function execute();

	public function output( $output ) {
		return $output;
	}
}
<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 29/10/2017
 * Time: 00:21
 */

namespace subsonic;

class stream extends abstract_api {
	public function execute() {
		return $this->args;
	}

	public function output( $output ) {
		$content = sprintf( '<audio controls src="data:audio/ogg;base64,%s"/>', base64_encode( $output ) );
		echo $content;
	}
}
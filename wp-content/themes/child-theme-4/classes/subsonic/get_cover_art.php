<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 29/10/2017
 * Time: 00:21
 */

namespace subsonic;

class get_cover_art extends abstract_api {
	public function execute() {
		return $this->args;
	}

	public function output( $output ) {
		$image=sprintf( '<img src="data:image/png;base64,%s"/>', base64_encode( $output ) );
		echo $image;
	}
}
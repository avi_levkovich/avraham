var vm = new Vue({
    el: '#generator',
    data: {
        title: null,
        slug: null,
        query: null,
        searchResults: [],
        chosenFromSearch: [],
        items: [],
        error: null
    },
    methods: {
        onSubmit: function (e) {
            var package = this.package();
            if (Object.keys(package).length === 0 && package.constructor === Object) {
                var errors=[];
                if(!this.title){
                    errors.push('missing title');
                }
                if(!this.slug){
                    errors.push('missing slug');
                }
                if(!this.items.length){
                    errors.push('missing items');
                }

                this.error=errors.join(', ');
                return false;
            } else {
                var self=this;
                jQuery.post(ajaxurl, {
                    action: 'add_playlist',
                    package: JSON.stringify(package)
                }, function (result) {
                    var output;
                    try {
                        output = JSON.parse(result);
                        if(output.url || output.error) {
                            if (output.url) {
                                window.location.assign(output.url);
                            }
                            if (output.error && output.error.errors) {
                                self.error = Object.keys(output.error.errors)[0];
                            }
                        }
                    } catch (e) {

                    }

                    if(!self.error){
                        self.error=result;
                    }
                });
            }
        },
        search: function () {
            var self = this;
            self.error=null;
            if (this.query) {
                jQuery.getJSON(ajaxobj.search + this.query, function (result) {
                    if (result && result['subsonic-response']) {
                        self.searchResults = result['subsonic-response'].searchResult2.song;
                        self.query = null;
                    }
                });
            }
        },
        choose: function () {
            var self = this;
            self.error=null;
            self.items = this.chosenFromSearch.map(function (value, index) {
                var id = value;
                var chosen = self.searchResults.find(function (value, index) {
                    return value.id == id;
                });
                return chosen;
            });
            self.searchResults = [];
            self.chosenFromSearch = [];

        },
        songId: function (id) {
            return 'song-' + id;
        },
        songTitle: function (result) {
            var year = result.year ? ('(' + result.year + ')') : '';
            return result.artist + ' - ' + result.title + year;
        },
        removeItem: function (id) {
            var self = this;
            self.error=null;
            var index = self.items.indexOf(self.items.find(function (value) {
                return value.id == id;
            }));
            this.items.splice(index, 1);
        },
        package: function () {
            if (this.title && this.slug && this.items.length) {
                return {
                    title: this.title,
                    slug: this.slug,
                    items: this.items
                };
            } else {
                return {};
            }
        }
    }
})
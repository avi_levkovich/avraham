<?php
/**
 * Created by PhpStorm.
 * User: Avi Levkovich (http://www.levkovich.co.il)
 * Date: 27/11/2017
 * Time: 23:59
 */

namespace admin;


class playlist_builder {
	protected $package;
	protected $title;
	protected $slug;
	protected $items;
	protected $id;

	/**
	 * playlist_builder constructor.
	 *
	 * @param $package
	 */
	public function __construct( $package ) {
		$this->package = $package;
	}

	public function in_items( $id ) {
		foreach ( $this->items as $item ) {
			$song = get_post( $id );
			if ( is_object( $item ) && $song->post_excerpt == $item->id ) {
				return true;
			}
		}

		return false;
	}

	public function build() {
		if ( is_object( $this->package ) ) {
			$title = isset( $this->package->title );
			$slug  = isset( $this->package->slug );
			$items = isset( $this->package->items );

			if ( $title ) {
				$this->title = sanitize_text_field( $this->package->title );
				if ( $slug ) {
					$this->slug = sanitize_title( $this->package->slug );
				} else {
					$this->slug = sanitize_title( $this->package->title );
				}
			}

			if ( $items && is_array( $this->package->items ) ) {
				$ids = array_map( array( $this, 'song_builder' ), $this->items = $this->package->items );
			}

			$post = array(
				'post_type'    => 'playlist',
				'post_title'   => $this->title,
				'post_name'    => $this->slug,
				'post_excerpt' => implode( ',', $ids ),
				'post_status'  => 'publish'
			);

			$this->id = wp_insert_post( $post, true );
		}

		return $this;
	}

	protected function song_builder( $item ) {
		$song = new song_builder( $item );

		return $song->getId();
	}

	/**
	 * @return mixed
	 */
	public function getId() {
		return $this->id;
	}

	protected function invalid_json() {
		return new \WP_Error( 'Not a valid json' );
	}

	public function get_edit_link() {
		if ( ! is_wp_error( $this->id ) ) {
			if ( $this->id ) {
				$output = array( 'url' => get_edit_post_link( $this->id ) );
			} else {
				$output = array( 'error' => $this->invalid_json() );
			}
		} else {
			$output = array( 'error' => $this->id );
		}

		return json_encode( $output );
	}


}
<?php
/**
 * Created by PhpStorm.
 * User: Avi Levkovich (http://www.levkovich.co.il)
 * Date: 28/11/2017
 * Time: 00:05
 */

namespace admin;


class song_builder {
	protected $item;
	protected $id;
	protected $title;
	protected $slug;
	protected $remote_id;

	/**
	 * song_builder constructor.
	 *
	 * @param $item
	 */
	public function __construct( $item ) {
		$this->item = $item;
		$this->build();
	}

	protected function build() {
		if ( ! is_object( $this->item ) ) {
			return false;
		}

		$this->title     = sprintf( '%s - %s', sanitize_text_field( $this->item->artist ), esc_html( $this->item->title ) );
		$this->slug      = sanitize_title( $this->title );
		$this->remote_id = sanitize_text_field( $this->item->id );

		$post = array(
			'post_type'    => 'song',
			'post_title'   => $this->title,
			'post_name'    => $this->slug,
			'post_excerpt' => $this->remote_id,
			'post_status' => 'publish'
		);

		$this->id=wp_insert_post($post,true);
	}

	/**
	 * @return mixed
	 */
	public function getId() {
		return $this->id;
	}

}
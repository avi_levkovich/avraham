<?php
/**
 * Created by PhpStorm.
 * User: Avi Levkovich (http://www.levkovich.co.il)
 * Date: 21/11/2017
 * Time: 20:26
 */

namespace admin;


class generator {
	use \testible;

	protected $input;
	protected $package;
	protected $playlist;

	/**
	 * generator constructor.
	 */
	public function __construct( $input = false ) {
		if ( $input && phpunit() ) {
			$this->input = $input;
		}
		add_action( 'admin_menu', array( $this, 'generator_add' ) );
		add_action( 'init', array( $this, 'register_post_types' ) );

		add_action( 'admin_enqueue_scripts', array( $this, 'enqueues' ) );
		add_action( 'wp_ajax_add_playlist', array( $this, 'add_playlist' ) );
	}

	public function get_post() {
		if ( $this->input ) {
			$post = $this->input;
		} else {
			$post = trim( file_get_contents( "php://input" ) );
		}

		return $post;
	}

	protected function invalid_json() {
		return new \WP_Error( 'Not a valid json' );
	}

	protected function setPackage( $package ) {
		$this->package = $package;
	}

	public function getPackage() {
		return $this->package;
	}

	protected function build_playlist( $output ) {
		if ( isset( $output['package'] ) ) {
			$this->package  = json_decode( $output['package'] );
			$playlist       = new playlist_builder( $this->package );
			$output         = $playlist->build()->get_edit_link();
			$this->playlist = $playlist->getId();

			return $output;
		} else {
			return $this->invalid_json();
		}
	}

	public function add_playlist() {
		$post = $this->get_post();
		parse_str( $post, $output );
		if ( isset( $output['package'] ) ) {
			$return = $this->build_playlist( $output );
			if ( is_wp_error( $return ) ) {
				$return = json_encode( array( 'error' => $return ) );
			}
		} else {
			$return = json_encode( array( 'error' => $this->invalid_json() ) );
		}
		if(phpunit()) {
			return $return;
		} else {
			exit( $return );
		}
	}

	public function register_post_types() {
		foreach (
			array(
				'playlist',
				'song'
			) as $post_type
		) {
			$func = sprintf( '%s_post_type_args', $post_type );
			register_post_type( $post_type, $this->$func( $post_type ) );
		}
	}

	protected function playlist_post_type_args( $post_type ) {
		return $args = array(
			'label'              => $post_type,
			'menu_position'      => 0,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => $post_type ),
			'capability_type'    => 'post',
			'has_archive'        => false,
			'hierarchical'       => false,
			'show_in_rest'       => false,
			'delete_with_user'   => true,
			'menu_icon'          => 'dashicons-media-audio',
			'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
		);
	}

	protected function song_post_type_args( $post_type ) {
		return $args = array(
			'label'              => $post_type,
			'menu_position'      => 1,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => $post_type ),
			'capability_type'    => 'post',
			'has_archive'        => false,
			'hierarchical'       => false,
			'show_in_rest'       => false,
			'delete_with_user'   => true,
			'menu_icon'          => 'dashicons-format-audio',
			'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
		);
	}

	public function generator_add() {
		add_menu_page(
			'Playlist Generator',
			'Playlist Generator',
			'manage_options',
			'generator',
			array( $this, 'generator' ),
			'dashicons-playlist-audio',
			6
		);
	}

	public function generator() {
		require_once __DIR__ . '/templates/generator.php';
	}

	protected function convert_slashes( $input ) {
		return str_replace( '\\', '/', $input );
	}

	public function enqueues( $hook ) {
		if ( 'toplevel_page_generator' != $hook ) {
			return;
		}

		$dir  = $this->convert_slashes( get_stylesheet_directory() );
		$uri  = $this->convert_slashes( get_stylesheet_directory_uri() );
		$path = $this->convert_slashes( __DIR__ . '/scripts/generator.js' );


		$generator = str_replace( $dir, $uri, $path );
		wp_enqueue_script( 'vue-js', 'https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.8/vue.js', array( 'jquery' ), '1.0.0', true );
		wp_enqueue_script( 'generator', $generator, array( 'jquery' ), '1.0.0', true );

		$array = array(
			'search' => home_url( 'slim/api/subsonic/search2?query=' ),
		);
		wp_localize_script( 'generator', 'ajaxobj', $array );
	}

	public function getPlaylist() {
		return $this->playlist;
	}


}
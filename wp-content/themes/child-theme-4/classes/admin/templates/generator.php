<form class="wrap" id="generator" v-on:submit.prevent="onSubmit">
    <h1 class="wp-heading-inline">Playlist Generator</h1>
    <div id="poststuff">
        <div id="post-body" class="metabox-holder columns-2">
            <div id="post-body-content" style="position: relative;">
                <div id="titlediv">
                    <div id="titlewrap">
                        <input type="text" name="post_title" v-model="title" size="30" value="" id="title"
                               spellcheck="true"
                               autocomplete="off"
                               placeholder="Enter title here">
                    </div>
                    <div class="inside">
                        <input type="text" v-model="slug" placeholder="slug">
                    </div>
                </div>
                <div>
                    <input type="text" v-model="query" placeholder="Enter Your Query">
                    <div class="button button-primary" v-on:click="search">Search</div>
                </div>
                <div v-if="searchResults.length">
                    <ul v-for="result in searchResults">
                        <li>
                            <input v-model="chosenFromSearch" type="checkbox" :id="songId(result.id)"
                                   :value="result.id">
                            <label :for="songId(result.id)" v-text="songTitle(result)"></label>
                        </li>
                    </ul>
                    <div class="button button-primary" v-on:click.prevent="choose">Choose</div>
                </div>

            </div><!-- /post-body-content -->
            <div id="postbox-container-1" class="postbox-container">
                <div id="side-sortables" class="meta-box-sortables ui-sortable" style="">
                    <div class="postbox">
                        <div class="inside">
                            <div>
                                <input type="submit" class="button button-primary button-large" value="Submit"/>
                            </div>
                            <div v-if="error">
                                <span v-text="error"></span>
                            </div>
                        </div>
                    </div>

                    <div class="postbox">
                        <div class="inside">
                            <table>
                                <tr v-for="item in items">
                                    <td>
                                        <span v-text="songTitle(item)"></span>
                                    </td>
                                    <td>
                                        <span class="button" v-on:click="removeItem(item.id)">Remove</span>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <br class="clear">
        </div>
    </div>
</form>
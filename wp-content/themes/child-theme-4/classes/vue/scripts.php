<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 23/09/2017
 * Time: 20:05
 */

namespace vue;


class scripts {
	protected $html;
	protected $scripts = array();
	protected $styles = array();

	/**
	 * scripts constructor.
	 */
	public function __construct() {
		add_action( 'wp_enqueue_scripts', array( $this, 'fecth_styles' ) );
		add_action( 'wp_head', function () {
			echo sprintf( '<base href="%s">', get_stylesheet_directory_uri() . '/dist/' );
		} );
	}

	public function fecth_styles() {
		if ( file_exists( get_stylesheet_directory() . '/dist/index.html' ) ) {
			$html = $this->html = file_get_contents( get_stylesheet_directory() . '/dist/index.html' );

			$re = '/<(link|style)(?=[^<>]*?(?:type="(text\/css)"|>))(?=[^<>]*?(?:media="([^<>"]*)"|>))(?=[^<>]*?(?:href="(.*?)"|>))(?=[^<>]*(?:rel="([^<>"]*)"|>))(?:.*?<\/\1>|[^<>]*>)
/i';

			preg_match_all( $re, $html, $matches );
			$css = isset( $matches[0] ) ? $matches[0] : false;

			$css = array_map( function ( $href ) {
				return trim( str_replace( array(
					'<link href=',
					'rel=stylesheet>'
				), '', $href ) );
			}, $css );

			$css = array_filter( $css, function ( $href ) {
				return ! is_numeric( $href );
			} );

			foreach ( $this->styles = $css as $href ) {
				wp_enqueue_style( sanitize_title( $href ), get_stylesheet_directory_uri() . '/dist' . $href );
			}


			wp_enqueue_style( 'bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css' );
			//wp_enqueue_style( 'bootstrap-vue', get_stylesheet_directory_uri() . '/views/css/bootstrap-vue.css' );

			$re = '/src=\/(.*)>/iU';

			preg_match_all( $re, $html, $matches, PREG_SET_ORDER, 0 );

			foreach ( $matches as $script ) {
				$this->scripts[] = $script[1];
				wp_enqueue_script( sanitize_title( $script[1] ), get_stylesheet_directory_uri() . '/dist/' . $script[1], array(), false, true );
			}

			//wp_enqueue_script( 'polyfill', get_stylesheet_directory_uri() . '/views/js/polyfill.min.js', array(), false, true );
			//wp_enqueue_script( 'bootstrap-vue', get_stylesheet_directory_uri() . '/views/js/bootstrap-vue.js', array(), false, true );
		}
	}
}
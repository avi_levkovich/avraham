<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 23/09/2017
 * Time: 20:50
 */

namespace vue;


class generate_template {
	protected $post;
	protected $data;
	protected $post_id;
	protected $path;
	protected $filename;
	protected $previous_filename;
	protected $hierarchy;
	protected $content;

	/**
	 * generate_template constructor.
	 *
	 * @param $post
	 * @param $update
	 * @param $post_id
	 */
	public function __construct( $post_id, $data ) {
		$this->post    = $data;
		$this->data    = $_POST;
		$this->post_id = $post_id;
	}

	protected function validate() {
		$valid   = true;
		$message = 20;

		if ( isset( $this->data['post_content'] ) && ! $this->data['post_content'] ) {
			$valid   = false;
			$message = 21;
		}

		if ( isset( $this->data['post_title'] ) && ! $this->data['post_title'] ) {
			$valid               = false;
			$message             = 22;
			$_POST['post_title'] = 'error: please insert a title';
		}

		if ( is_numeric( substr( $this->data['post_title'], 0, 1 ) ) ) {
			$valid   = false;
			$message = 23;
		}

		if ( isset( $this->data['tax_input'] ) && isset( $this->data['tax_input']['app'] ) && isset( $this->data['tax_input']['app'][0] ) && count( $this->data['tax_input']['app'] ) == 1 ) {
			$valid   = false;
			$message = 24;
		}

		$app = $this->data['tax_input']['app'];

		$app = array_filter( $app, function ( $id ) {
			$term = get_term_by( 'id', $id, 'app', ARRAY_A );

			return $id && $term['parent'];
		} );

		$app = array_values( $app );
		$app = array_pop( $app );

		$app = get_term_by( 'id', $app, 'app', ARRAY_A );

		wp_set_object_terms( $this->post_id, $app['name'], 'app' );

		if ( $valid && ! $app ) {
			$valid   = false;
			$message = 25;
		}

		$term            = $app;
		$this->hierarchy = array( $app['name'] );
		while ( $term['parent'] ) {
			$term = get_term_by( 'id', $term['parent'], 'app', ARRAY_A );
			array_unshift( $this->hierarchy, $term['name'] );
		}

		$this->hierarchy[0] = 'components';

		add_filter( 'redirect_post_location', function ( $location, $post_id ) use ( $message ) {
			return str_replace( "message=1", sprintf( 'message=%d', $message ), $location );
		}, 10, 2 );

		return $valid;
	}

	public function dismiss() {

	}

	public function process() {
		$this->path = get_stylesheet_directory() . '/src';
		foreach ( $this->hierarchy as $folder ) {
			$this->path .= '/' . $folder;
			if ( ! file_exists( $this->path ) ) {
				mkdir( $this->path );
			}
		}

		$this->filename = ucfirst( sanitize_title( $this->data['post_title'] ) );

		$post                    = get_post( $this->post_id );
		$this->previous_filename = ucfirst( sanitize_title( $post->post_title ) );

		remove_filter( 'the_content', 'wpautop' );
		//$template = do_shortcode($this->data['post_content']);
		$template = apply_filters( 'the_content', $this->data['post_content'] );
		$template = preg_replace( "/<script.*?\/script>/s", "", $template ) ?: $template;


		add_filter( 'the_content', 'wpautop' );

		$file = sprintf( '%s/%s.vue', $this->path, $this->previous_filename );


		if ( file_exists( $file ) ) {
			$content = file_get_contents( $file );
			$re      = '/<script>(.*?)<\/script>/s';
			preg_match( $re, $content, $matches, PREG_OFFSET_CAPTURE, 0 );
			$script = ( isset( $matches[1] ) && isset( $matches[1][0] ) ) ? $matches[1][0] : false;

			$re = '/<style(.*?)>(.*?)<\/style>/s';
			preg_match( $re, $content, $matches, PREG_OFFSET_CAPTURE, 0 );

			$style = ( isset( $matches[2] ) && isset( $matches[2][0] ) ) ? $matches[2][0] : false;

			$script = $script ? $script : '\r\nexport default {\r\n\r\n}\r\n';

			$this->content = sprintf( '<template>\r\n%s\r\n</template>\r\n%s%s', $template, $script, $style );
		}

		$script = ( isset( $script ) && $script ) ? sprintf( '\r\n<script>%s</script>\r\n', $script ) : sprintf( '\r\n<script>\r\n%s\r\n</script>\r\n', 'export default {\r\n\r\n}' );
		$style  = ( isset( $style ) && $style ) ? sprintf( '\r\n<style scoped>%s</style>\r\n', $style ) : '';

		$this->content = str_replace( '\r\n', PHP_EOL, sprintf( '<template>\r\n%s\r\n</template>\r\n%s%s', $template, $script, $style ) );
		$this->content = str_replace('”','"',html_entity_decode(stripcslashes ($this->content )));
	}

	public function isValid() {
		return $this->validate();
	}

	public function export() {
		$filepath = sprintf( '%s/%s.vue', $this->path, $this->previous_filename );
		if ( $this->filename !== $this->previous_filename && file_exists( $filepath ) ) {
			unlink( $filepath );
		}

		$file    = sprintf( '%s/%s.vue', $this->path, $this->filename );
		$content = do_shortcode( $this->content );
		file_put_contents( $file, $content );
	}
}
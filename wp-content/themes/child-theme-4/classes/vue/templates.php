<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 23/09/2017
 * Time: 20:12
 */

namespace vue;


class templates {

	/**
	 * templates constructor.
	 */
	public function __construct() {
		add_action( 'init', array( $this, 'vue_template_init' ) );
		add_action( 'pre_post_update', array( $this, 'save_post_vue' ), 10, 2 );
		add_action( 'admin_notices', array( $this, 'vue_template_saved' ) );
	}

	public function vue_template_saved() {
		if ( ! isset( $_GET['message'] ) || ! isset( $_GET['post'] ) ) {
			return;
		}

		if(isset($_GET['post'])){
			$post=get_post($_GET['post']);
		}

		if(isset($post) && $post->post_type=='vue') {
			if ( $_GET['message'] == 20 ) {
				$vue = 1;
			} else {
				$vue = 0;
				switch ( $_GET['message'] ) {
					case 21:
						$reason = 'no post_content';
						break;
					case 22:
						$reason = 'no post_title';
						break;
					case 23:
						$reason = 'post_title begins with a number';
						break;
					case 24:
						$reason = 'no app term was chosen';
						break;
					case 25:
						$reason = 'app term must not be top-level';
						break;
				}
			}

			if ( isset( $vue ) ) {
				$post     = get_post( $_GET['post'] );
				$class    = sprintf( 'notice notice-%s', $vue ? 'success' : 'error' );
				$filename = ucfirst( sanitize_title( $post->post_title ) );
				$fail     = ( $filename && ! $vue && isset( $reason ) ) ? sprintf( '%s.Vue has failed to save. %s', $filename, $reason ) : 'no Vue file saved, no title';
				$message  = $vue ? sprintf( '%s.Vue has save successfully', $filename ) : $fail;

				if ( $message ) {
					echo sprintf( '<div class="%s"><p>%s</p></div>', esc_attr( $class ), esc_html( $message ) );
				}

				echo '<div class="notice notice-success"><p>Post Saved</p></div>';
			}
		}
	}

	public function vue_template_init() {
		$labels = array(
			'name'              => 'Apps',
			'singular_name'     => 'App',
			'search_items'      => 'Search Apps',
			'all_items'         => 'All Apps',
			'parent_item'       => 'Parent App',
			'parent_item_colon' => 'Parent App:',
			'edit_item'         => 'Edit App',
			'update_item'       => 'Update App',
			'add_new_item'      => 'Add New App',
			'new_item_name'     => 'New Genre App',
			'menu_name'         => 'App',
		);

		$args = array(
			'hierarchical'       => true,
			'description'        => 'App',
			'public'             => false,
			'publicly_queryable' => false,
			'labels'             => $labels,
			'show_ui'            => true,
			'show_admin_column'  => true,
			'query_var'          => false,
			'rewrite'            => false,
		);

		register_taxonomy( 'app', array( 'vue' ), $args );

		$labels = array(
			'name'               => 'Vue Templates',
			'singular_name'      => 'Vue Template',
			'menu_name'          => 'Vue Templates',
			'name_admin_bar'     => 'Vue Template',
			'add_new'            => 'Add New',
			'add_new_item'       => 'Add New Vue Template',
			'new_item'           => 'New Vue Template',
			'edit_item'          => 'Edit Vue Template',
			'view_item'          => 'View Vue Template',
			'all_items'          => 'All Vue Templates',
			'search_items'       => 'Search Vue Templates',
			'parent_item_colon'  => 'Parent Vue Templates:',
			'not_found'          => 'No Vue Templates found.',
			'not_found_in_trash' => 'No Vue Templates found in Trash.',
		);

		$args = array(
			'labels'              => $labels,
			'description'         => 'Vue Template',
			'public'              => false,
			'publicly_queryable'  => false,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'query_var'           => false,
			'can_export'          => true,
			'rewrite'             => false,
			'capability_type'     => 'post',
			'has_archive'         => true,
			'hierarchical'        => false,
			'menu_position'       => null,
			'exclude_from_search' => true,
			'supports'            => array( 'title', 'editor' ),
			'taxonomies'          => array( 'app' )
		);

		register_post_type( 'vue', $args );
	}

	public function save_post_vue( $post_id, $post ) {
		if ( $post['post_type'] == 'vue' ) {
			$generate_template = new \vue\generate_template( $post_id, $post );
			if ( $generate_template->isValid() ) {
				$generate_template->process();
				$generate_template->export();
			} else {
				$generate_template->dismiss();
			}
		}
	}
}
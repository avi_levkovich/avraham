<?php
/**
 * Created by PhpStorm.
 * User: Avi Levkovich (http://www.levkovich.co.il)
 * Date: 20/01/2018
 * Time: 22:11
 */

class avraham_postmeta_archive extends avraham_archive_abstract_list_table {
	public function table_name() {
		return 'archive_meta';
	}

	protected function singular() {
		return 'Archive Meta Item';
	}

	protected function plural() {
		return 'Archive Meta Items';
	}

	public function title() {
		return "Postmeta";
	}

	public function slug() {
		return 'postmeta_archive';
	}
}
<?php
/**
 * Created by PhpStorm.
 * User: Avi Levkovich (http://www.levkovich.co.il)
 * Date: 20/01/2018
 * Time: 22:11
 */

class avraham_posts_archive extends avraham_archive_abstract_list_table {

	public function table_name() {
		return 'archive';
	}

	protected function singular() {
		return 'Archive Post Item';
	}

	protected function plural() {
		return 'Archive Post Items';
	}

	public function title() {
		return "Posts";
	}

	public function slug() {
		return 'posts_archive';
	}
}
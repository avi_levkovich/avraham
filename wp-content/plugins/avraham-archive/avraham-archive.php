<?php
/**
 * Plugin Name: Avraham Archive
 * Version:
 * Description:
 * Author: Avi Levkovich
 * Author URI: http://levkovich.co.il/
 */

add_action( 'init', function () {
	new avraham_archive();
} );


class avraham_archive {

	const TABLE = 'archive';
	const DAYS = 7;

	const FIELDS = array(
		'ID',
		'post_author',
		'post_date',
		'post_content',
		'post_title',
		'post_excerpt',
		'post_name',
		'post_modified',
		'guid',
		'menu_order',
		'post_type'
	);

	const META_FIELDS = array(
		'meta_id',
		'post_id',
		'meta_key',
		'meta_value'
	);

	protected $charset_collate;
	protected $max_index_length;
	protected $list;
	protected $title;
	protected $edit;

	public function __construct() {
		$this->check_if_table_exists();
		$this->move_to_archive();

		add_filter( 'set-screen-option', array( $this, 'set_screen' ), 10, 3 );
		add_filter( 'posts_pre_query', array( $this, 'posts_pre_query' ), 10, 2 );
		add_action( 'admin_menu', array( $this, 'add_list_table_page' ) );

		$page       = $_GET['page'] ?? null;

		$action = $_REQUEST['action'] ?? null;

		if($action=='restore'){
//		    $referer=$_REQUEST['_wp_http_referer']??null;
//			$re = '/^.*page=(.*).php.*/';
//			preg_match_all($re, $referer, $matches, PREG_SET_ORDER, 0);
//            if(count($matches)){
//                $page=$matches[0][1];
//            }
            $page='posts_archive';
		}

		$this->edit = $_GET['id'] ?? null;
		$class      = rtrim( 'avraham_' . $page, '.php' );
		$file       = sprintf( '%s/%s.php', dirname( __FILE__ ), $class );

		if ( file_exists( $file ) ) {
			require_once 'avraham_archive_abstract_list_table.php';
			require_once $file;
			$this->list = new $class();

			if ( $action == 'update' ) {
				$this->list->update();
				$this->redirect();
			}

			if ( $page && file_exists( $file ) ) {
				if ( ! $this->edit ) {
					$this->list->prepare_items();
				} else {
					if ( $this->list->is_empty() ) {
						$this->redirect();
						//$this->list->prepare_items();
					}
				}
			}
		}

		$screen = rtrim( $page, '.php' );
		if($screen=='posts_archive') {
			add_filter( 'bulk_actions-' . $screen, function ( $bulk_actions ) {
				$bulk_actions['restore'] = 'Restore';

				return $bulk_actions;
			} );
		}


		if($action=='restore'){
		    $post_ids=$_REQUEST['bulk-delete'] ?? null;
		    if($post_ids && is_array($post_ids)) {
			    $this->restore( $post_ids );
		    }
			$this->redirect();
        }


	}

	protected function restore( $post_ids ) {
	    global $wpdb;


		foreach ($post_ids as $id){
		    $this->list->restore($id);
        }
	}

	protected function redirect() {
		$url  = admin_url( 'admin.php?page=posts_archive.php' );
		wp_redirect( $url, 301 );
	}

	public function add_list_table_page() {
		if ( $this->list ) {
			$this->title = $this->list->title();
		}

		foreach ( array( 'posts_archive', 'postmeta_archive' ) as $slug ) {
			$title = str_replace( 'avraham_', '', $slug );
			$title = implode( ' ', array_map( function ( $val ) {
				return ucfirst( $val );
			}, explode( '_', $title ) ) );

			$hook = add_menu_page(
				$title,
				$title,
				'manage_options',
				$slug . '.php',
				array(
					$this,
					'list_table_page'
				)
			);

			add_action( "load-$hook", [ $this, 'screen_option' ] );
		}
	}

	public static function set_screen( $status, $option, $value ) {
		return $value;
	}

	public function screen_option() {
		if ( ! class_exists( 'WP_List_Table' ) ) {
			require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
		}
	}

	public function list_table_page() {
		if ( $this->list ) {
			?>
            <div class="wrap">
				<?php
				if ( ! $this->edit || $this->list->is_empty() ) {
				?>
                <h2><?php echo $this->title; ?> Archive List Table Page</h2>
                <form method="get">
					<?php $this->list->display(); ?>
					<?php
					} else {
						$this->list->edit();
					}
					?>
                </form>
            </div>
			<?php
		}
	}

	public function posts_pre_query( $something, $query ) {
		if ( $query->query_vars['name'] ) {
			global $wpdb;
			$results = $wpdb->get_row( $query->request );
			if ( $results ) {
				return null;
			} else {
				if ( ! $query->query_vars['post_type'] || $query->query_vars['post_type'] == 'post' ) {
					$request = $query->request;
					$request = str_replace( $wpdb->posts, $wpdb->prefix . self::TABLE, $request );
					$request = str_replace( $wpdb->postmeta, $wpdb->prefix . self::TABLE . '_meta', $request );
					$results = $wpdb->get_results( $request );

					return $results;
				}
			}
		}

		return null;
	}

	protected function create_table( $table ) {
		global $wpdb;

		$tables = $wpdb->get_col( $wpdb->prepare( "SHOW TABLES LIKE %s", $table ) );

		if ( ! count( $tables ) ) {
			$method  = sprintf( '%s_query', $table );
			$query   = $this->$method();
			$dbdelta = dbDelta( $query );

			return true;
		} else {
			return false;
		}
	}

	protected function archive_query() {
		global $wpdb;
		$table = $wpdb->prefix . self::TABLE;

		$query = "CREATE TABLE $table (
				  ID bigint(20) unsigned NOT NULL auto_increment,
				  post_author bigint(20) unsigned NOT NULL default '0',
				  post_date datetime NOT NULL default '0000-00-00 00:00:00',  
				  post_content longtext NOT NULL,
				  post_title text NOT NULL,
				  post_excerpt text NOT NULL,  
				  post_name varchar(200) NOT NULL default '',  
				  post_modified datetime NOT NULL default '0000-00-00 00:00:00',    
				  guid varchar(255) NOT NULL default '',
				  menu_order int(11) NOT NULL default '0',
				  post_type varchar(20) NOT NULL default 'post',  
				  PRIMARY KEY  (ID),
				  KEY post_name (post_name($this->max_index_length)),    
				  KEY post_author (post_author)
				) $this->charset_collate;\n";

		return $query;
	}

	protected function archive_meta_query() {
		global $wpdb;
		$table = $wpdb->prefix . self::TABLE . '_meta';

		$query = "CREATE TABLE $table (
				  meta_id bigint(20) unsigned NOT NULL auto_increment,
				  post_id bigint(20) unsigned NOT NULL default '0',
				  meta_key varchar(255) default NULL,
				  meta_value longtext,
				  PRIMARY KEY  (meta_id),
				  KEY post_id (post_id),
				  KEY meta_key (meta_key($this->max_index_length))
				) $this->charset_collate;";

		return $query;
	}

	public function check_if_table_exists() {
		global $wpdb;

		$this->max_index_length = 191;
		$this->charset_collate  = $wpdb->get_charset_collate();

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

		$created = true;

		foreach ( array( self::TABLE, self::TABLE . '_meta' ) as $table ) {
			$created &= $this->create_table( $table );
		}

		return $created;
	}

	public
	function move_to_archive() {
		$ids = $this->copy_from_posts_to_archive();
		$this->copy_from_post_meta_to_archive_meta( $ids );
	}

	protected function copy_from_post_meta_to_archive_meta( $ids ) {
		if ( is_array( $ids ) && count( $ids ) ) {
			global $wpdb;
			$query  = sprintf( "SELECT * FROM $wpdb->postmeta WHERE post_id IN (%s)", trim( implode( ',', $ids ), '\'' ) );
			$metas  = $wpdb->get_results( $query, ARRAY_A );
			$values = array_map( array( $this, 'archive_meta_values' ), $metas );

			if(count($values)) {
				$fields = sprintf( '(%s)', implode( ',', self::META_FIELDS ) );
				$query  = sprintf( "INSERT IGNORE INTO %s %s VALUES %s", $wpdb->prefix . self::TABLE . '_meta', $fields, implode( ',', $values ) );

				$affected = $wpdb->query( $query );

				if ( $affected ) {
					$query   = sprintf( "DELETE FROM `$wpdb->postmeta` WHERE post_id IN (%s)", implode( ',', $ids ) );
					$deleted = $wpdb->query( $query );
				}
			}
		}

		return true;
	}

	protected function copy_from_posts_to_archive() {
		global $wpdb;
		$query = sprintf( "SELECT * FROM `$wpdb->posts` WHERE post_type='post' AND DATE(post_date) < DATE_SUB(CURDATE(), INTERVAL %d DAY)", self::DAYS );

		$query = apply_filters( 'archive_query_critearia', $query );

		$posts = $wpdb->get_results( $query, ARRAY_A );

		$ids = array_column( $posts, 'ID' );

		if ( count( $ids ) ) {
			$values = array_map( array( $this, 'archive_values' ), $posts );

			$fields = sprintf( '(%s)', implode( ',', self::FIELDS ) );
			$query  = sprintf( "INSERT INTO %s %s VALUES %s ON DUPLICATE KEY UPDATE ID=ID", $wpdb->prefix . self::TABLE, $fields, implode( ',', $values ) );

			$affected = $wpdb->query( $query );

			if ( $affected ) {
				$query   = sprintf( "DELETE FROM `$wpdb->posts` WHERE ID IN (%s)", implode( ',', $ids ) );
				$deleted = $wpdb->query( $query );
			}
		}

		return $ids;
	}

	protected function archive_meta_values( $post ) {
		foreach ( $post as $key => $field ) {
			if ( ! in_array( $key, SELF::META_FIELDS ) ) {
				unset( $post[ $key ] );
			} else {
				$post[ $key ] = '"' . $post[ $key ] . '"';
			}
		}

		$return = sprintf( '(%s)', implode( ',', $post ) );

		return $return;
	}

	protected function archive_values( $post ) {
		foreach ( $post as $key => $field ) {
			if ( ! in_array( $key, SELF::FIELDS ) ) {
				unset( $post[ $key ] );
			} else {
				$post[ $key ] = '"' . $post[ $key ] . '"';
			}
		}

		$return = sprintf( '(%s)', implode( ',', $post ) );

		return $return;
	}
}
<?php
/**
 * Created by PhpStorm.
 * User: Avi Levkovich (http://www.levkovich.co.il)
 * Date: 22/01/2018
 * Time: 21:50
 */

abstract class avraham_archive_abstract_list_table extends WP_List_Table {
	protected $columns;
	protected $primary;
	protected $screen;
	protected $edit;
	static protected $table;

	public function __construct() {
		global $wpdb;
		self::$table = $wpdb->prefix . $this->table_name();

		$describe = sprintf( "DESCRIBE %s", self::$table );
		$data     = $wpdb->get_results( $describe, ARRAY_A );

		$this->columns = array_column( $data, 'Field' );
		$this->primary = array_search( 'PRI', array_column( $data, 'Key' ) );

		require_once 'avraham_archive_edit.php';
		$this->edit = new avraham_archive_edit( $this );

		parent::__construct( [
			'singular' => $this->singular(),
			'plural'   => $this->plural(),
			'ajax'     => false,
			'screen'   => $this->slug()
		] );
	}

	abstract public function table_name();

	abstract protected function singular();

	abstract protected function plural();

	abstract public function title();

	abstract public function slug();

	public function restore( $post_id ) {
		global $wpdb;
		$prepared = $wpdb->prepare( "SELECT * FROM {$wpdb->prefix}archive WHERE ID=%d", $post_id );
		$post     = $wpdb->get_row( $prepared, ARRAY_A );
		if ( ! $post ) {
			return;
		}
		$prepared = $wpdb->prepare( "SELECT * FROM {$wpdb->prefix}archive_meta WHERE post_id=%d", $post_id );
		$metas    = $wpdb->get_results( $prepared );

		$values = array_map( function ( $meta ) {
			$mapped = array_map( function ( $item ) {
				return sprintf( '"%s"', $item );
			}, array_values( (array) $meta ) );

			return sprintf( '(%s)', implode( ',', $mapped ) );
		}, $metas );
		$values = implode( ',', $values );
		$ids    = array_column( $metas, 'meta_id' );

		$insert_sql = sprintf( "INSERT INTO $wpdb->postmeta (meta_id,post_id,meta_key,meta_value) VALUES %s", $values );

		$delete_sql = sprintf( "DELETE FROM $wpdb->postmeta WHERE meta_id IN (%s)", implode( ',', $ids ) );

		$insert_meta = $wpdb->query( $insert_sql );
		$delete_meta = $wpdb->query( $delete_sql );
		$deleted     = self::delete_one( $post_id );

		$postdata = $post;
		unset( $postdata['ID'] );

		$postdata = apply_filters( 'modify_data_before_restore_from_archive', $postdata );

		$insert = wp_insert_post( $postdata, true );

		do_action( 'after_restored_from_archive', $postdata );
	}

	public function is_empty() {
		return $this->edit->is_empty();
	}

	public function edit() {
		$edit = $this->edit;

		if ( ! $this->is_empty() ) {
			$edit->view();
		}
	}

	public function update() {
		$this->edit->update();
	}

	public static function get_all( $per_page = 5, $page_number = 1 ) {
		global $wpdb;
		$sql = sprintf( "SELECT * FROM %s", self::$table );
		if ( ! empty( $_REQUEST['orderby'] ) ) {
			$sql .= ' ORDER BY ' . esc_sql( $_REQUEST['orderby'] );
			$sql .= ! empty( $_REQUEST['order'] ) ? ' ' . esc_sql( $_REQUEST['order'] ) : ' ASC';
		}
		$sql    .= " LIMIT $per_page";
		$sql    .= ' OFFSET ' . ( $page_number - 1 ) * $per_page;
		$result = $wpdb->get_results( $sql, 'ARRAY_A' );

		return $result;
	}

	public static function delete_one( $id ) {
		global $wpdb;
		$delete = $wpdb->delete(
			self::$table,
			array( 'id' => $id ),
			array( '%d' )
		);

		return $delete;
	}

	public static function record_count() {
		global $wpdb;
		$count = $wpdb->get_var( sprintf( "SELECT COUNT(*) FROM %s", self::$table ) );

		return $count;
	}

	public function no_items() {
		echo apply_filters( "no_items_" . self::$table, sprintf( "No %s items found", self::$table ) );
	}

	function column_name( $item ) {
		$delete_nonce = wp_create_nonce( "delete_" . self::$table );
		$title        = '<strong>' . $item['name'] . '</strong>';
		$actions      = [
			'delete' => sprintf( '<a href="?page=%s&action=%s&' . self::$table . '=%s&_wpnonce=%s">Delete</a>', esc_attr( $_REQUEST['page'] ), 'delete', absint( $item['ID'] ), $delete_nonce )
		];

		return $title . $this->row_actions( $actions );
	}

	public function column_default( $item, $column_name ) {
		if ( $column_name == $this->columns[ $this->primary ] ) {
			$url = admin_url( sprintf( "admin.php?page=%s&id=%s", $_GET['page'], $item[ $column_name ] ) );

			return sprintf( '<a href="%s">%s</a>', $url, $item[ $column_name ] );
		} else {
			return wp_trim_words( $item[ $column_name ] );
		}
	}

	public function column_cb( $item ) {
		return sprintf(
			'<input type="checkbox" name="bulk-delete[]" value="%s" />', $item[ $this->columns[0] ]
		);
	}

	public function get_columns() {
		$columns = array( 'cb' => '<input type="checkbox" />' );
		foreach ( $this->columns as $column ) {
			$columns[ $column ] = $column;

		}

		return $columns;
	}

	public function get_sortable_columns() {
		$sortable_columns = array_combine( array_values( $this->columns ), array_values( $this->columns ) );

		return $sortable_columns;
	}

	public function get_bulk_actions() {
		$actions = [
			'bulk-delete' => 'Delete'
		];

		return $actions;
	}

	public function prepare_items() {

		$this->_column_headers = $this->get_column_info();

		/** Process bulk action */
		$this->process_bulk_action();

		$per_page     = $this->get_items_per_page( self::$table, 10 );
		$current_page = $this->get_pagenum();
		$total_items  = self::record_count();

		$this->set_pagination_args( [
			'total_items' => $total_items, //WE have to calculate the total number of items
			'per_page'    => $per_page //WE have to determine how many items to show on a page
		] );


		$this->items = self::get_all( $per_page, $current_page );
	}

	public function process_bulk_action() {

		//Detect when a bulk action is being triggered...
		if ( 'delete' === $this->current_action() ) {

			// In our file that handles the request, verify the nonce.
			$nonce = esc_attr( $_REQUEST['_wpnonce'] );

			if ( ! wp_verify_nonce( $nonce, self::$table ) ) {
				die( 'Go get a life script kiddies' );
			} else {
				self::delete_one( absint( $_GET[ self::$table ] ) );

				header( "Refresh:0" );
				exit;
			}

		}

		// If the delete bulk action is triggered
		if ( ( isset( $_POST['action'] ) && $_POST['action'] == 'bulk-delete' )
		     || ( isset( $_POST['action2'] ) && $_POST['action2'] == 'bulk-delete' )
		) {

			$delete_ids = esc_sql( $_POST['bulk-delete'] );

			// loop over the array of record IDs and delete them
			foreach ( $delete_ids as $id ) {
				self::delete_one( $id );

			}

			header( "Refresh:0" );
			exit;
		}
	}
}
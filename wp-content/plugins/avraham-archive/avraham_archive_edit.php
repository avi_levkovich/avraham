<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 05/09/2017
 * Time: 23:07
 */

class avraham_archive_edit {
	protected $id;
	protected $class;
	protected $table;
	protected $instance;
	protected $columns;
	protected $primary;
	protected $types;
	protected $empty;

	/**
	 * edit constructor.
	 *
	 * @param $id
	 */
	public function __construct( $list_table ) {
		add_filter( 'orm_edit_editor', array( $this, 'orm_edit_editor' ), 10, 3 );

		$this->init( $list_table );
	}

	public function orm_edit_editor( $return, $table, $field ) {
		global $wpdb;
		if ( $table == $wpdb->prefix . 'archive' ) {
			switch ( $field ) {
				case 'post_content':
				case 'post_excerpt':
					return true;
			}
		}

		return $return;
	}

	protected function init( $list_table ) {
		if ( ! isset( $_GET['id'] ) ) {
			return;
		}
		$this->id = $_GET['id'];

		global $wpdb;
		$this->table = $wpdb->prefix . $list_table->table_name();

		$data          = $wpdb->get_results( "DESCRIBE $this->table" );
		$this->columns = array_column( $data, 'Field' );
		$this->primary = array_search( 'PRI', array_column( $data, 'Key' ) );
		$this->types   = array_column( $data, 'Type' );

		$post = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM $this->table WHERE {$this->columns[$this->primary]}=%d", $this->id ) );

		if ( ! $post ) {
			$this->empty = true;

			return;
		}

		$this->instance = (array) $post;


	}

	protected function get_template( $template ) {
		$current_dir = dirname( __FILE__ );
		$filename    = "$current_dir/templates/$template.php";
		if ( file_exists( $filename ) ) {
			return file_get_contents( $filename );
		} else {
			throw new \Exception( 'file is missing' );
		}
	}

	/**
	 * @return mixed
	 */
	public function is_empty() {
		return $this->empty;
	}

	public function view() {
		$fields = array();

		foreach ( $this->columns as $key => $value ) {
			$type = str_replace( array(
				0,
				1,
				2,
				3,
				4,
				5,
				6,
				7,
				8,
				9,
				'(',
				')'
			), array_fill( 0, 11, '' ), $this->types[ $key ] );

			if ( strpos( $type, 'int' ) !== false ) {
				$type = 'int';
			}

			if ( strpos( $type, 'float' ) !== false ) {
				$type = 'float';
			}

			if ( strpos( $type, 'text' ) !== false || strpos( $type, 'varchar' ) !== false ) {
				$type = 'text';
				if ( apply_filters( 'orm_edit_editor', false, $this->table, $value ) ) {
					$type = 'editor';
				}
			}

			if ( $this->columns[ $this->primary ] == $value ) {
				$fields[ $value ] = $this->primary_field( $value );
			} else {
				switch ( $type ) {
					case 'editor':
						$fields[ $value ] = $this->editor( $value, true );
						break;
					case 'text':
						if ( strpos( $value, 'title' ) !== false ) {
							$fields[ $value ] = $this->title( $value );
						} else {
							$fields[ $value ] = $this->editor( $value );
						}
						break;
					case 'int':
						$fields[ $value ] = $this->number( $value, 'int' );
						break;
					case 'float':
						$fields[ $value ] = $this->number( $value, 'float' );
						break;
					case 'timestamp':
					case 'datetime':
						$fields[ $value ] = $this->timestamp( $value );
						break;
					default:
						break;
				}
			}
		}

		echo $this->content( $fields );

	}

	protected function content( $fields ) {
		$content = $this->get_template( 'edit' );
		$url     = admin_url( sprintf( "admin.php?page=%s.php&id=new", $this->table ) );

		$content = str_replace( '#url', $url, $content );
		$content = str_replace( 'Edit class', sprintf( 'Edit %s', str_replace( '_', ' ', apply_filters( "{$this->class}_name", $this->class ) ) ), $content );
		$content = str_replace( 'fields_placeholder', implode( '', $fields ), $content );
		$content = str_replace( 'value="action"', 'value="update"', $content );

		return $content;
	}

	protected function wp_editor( $content, $field ) {
		wp_editor( $content, $field, array(
			'_content_editor_dfw' => "{$this->class}_editor_{$field}'",
			'drag_drop_upload'    => true,
			'tabfocus_elements'   => 'content-html,save-post',
			'editor_height'       => 300,
			'tinymce'             => array(
				'resize'             => false,
				'wp_autoresize_on'   => true,
				'add_unload_trigger' => false,
			),
		) );
	}

	protected function editor( $field, $wp_editor = false ) {
		$content = $this->instance[ $field ];
		ob_start();
		echo sprintf( '<p for="%s">Enter %s here</p>', $field, apply_filters( "{$this->class}_field_$field", "$field" ) );
		if ( $wp_editor ) {
			$this->wp_editor( $content, $field );
		} else {
			echo sprintf( '<textarea id="%s" name="%s" rows="6" style="width:100%%;">%s</textarea>', $field, $field, $content );
		}

		return ob_get_clean();
	}

	protected function title( $field ) {
		$value = $this->instance[ $field ];
		$title = $this->get_template( 'title' );
		$title = str_replace( 'id_id', $field, $title );
		$title = str_replace( 'value_value', $value, $title );
		$title = str_replace( 'filtered', apply_filters( "{$this->class}_field_$field", $field ), $title );

		return $title;
	}

	protected function number( $field, $type ) {
		$value   = $this->instance[ $field ];
		$number  = $this->get_template( 'number' );
		$number  = str_replace( 'id_id', $field, $number );
		$number  = str_replace( 'value_value', $value, $number );
		$step    = apply_filters( "{$this->class}_step_$field", 0.1 );
		$pattern = $type == 'float' ? sprintf( 'pattern="%s" step="%s" ', '[0-9]+([\.,][0-9]+)?', $step ) : '';
		$number  = str_replace( '{pattern}', $pattern, $number );
		$number  = str_replace( 'filtered', apply_filters( "{$this->class}_field_$field", $field ), $number );
		$number  = str_replace( 'input_title', apply_filters( "{$this->class}_input_title_$field", "This should be a number with up to $step decimal places." ), $number );

		return $number;
	}

	protected function timestamp( $field ) {
		$input = $this->title( $field );

		return $input;
	}

	protected function primary_field( $field ) {
		$value = $this->instance[ $field ];
		$input = sprintf( '<input type="hidden" name="%s" value="%s" disabled>', $field, $value );

		return $input;
	}

	public function update() {
		if ( ! isset( $_REQUEST['action'] ) ) {
			return false;
		}

		$this->update_post( $this->id, $this->prepare() );

		return true;
	}

	protected function update_post( $id, $array ) {
		global $wpdb;
		list( $data, $types ) = $this->prepare_to_db( $array );

		return $wpdb->update(
			$this->table,
			$data,
			array( $this->columns[ $this->primary ] => $id ),
			$types,
			array( '%d' )
		);
	}

	protected function prepare_to_db( $array, $action = false ) {
		$primary = $this->primary;
		$data    = array();
		$types   = array();

		foreach ( $this->columns as $key => $column ) {
			if ( isset( $array[ $column ] ) && $key != $primary ) {
				$data[ $column ]  = $array[ $column ];
				$types[ $column ] = $this->types[ array_search( $column, $this->columns ) ];
			}
		}

		$types = array_map( function ( $type ) {
			if ( strpos( $type, 'int' ) !== false ) {
				$type = 'int';
			}

			if ( strpos( $type, 'float' ) !== false ) {
				$type = 'float';
			}

			if ( strpos( $type, 'text' ) !== false || strpos( $type, 'varchar' ) !== false ) {
				$type = 'text';
			}

			if ( strpos( $type, 'datetime' ) !== false ) {
				$type = 'timestamp';
			}

			switch ( $type ) {
				case 'int':
					return '%d';
				case 'float':
					return '%f';
				case 'text':
					return '%s';
				case 'timestamp':
					return '%s';
				default:
					return false;
			}

			return false;
		}, $types );

		return array( $data, $types );
	}

	protected function prepare() {
		$array = array();
		foreach ( $this->columns as $column ) {
			if ( isset( $_REQUEST[ $column ] ) ) {
				$array[ $column ] = $_REQUEST[ $column ];
			}
		}

		return $array;
	}
}
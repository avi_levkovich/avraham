<?php
/*
Plugin Name: Double Users
Author: Avi Levkovich
Author URI: https://levkovich.co.il/
*/

class double_user {
	/**
	 * double_user constructor.
	 */
	public function __construct() {
		add_filter( 'registration_errors', array( $this, 'registration_errors' ), 10, 3 );
	}

	public function registration_errors( $errors, $sanitized_user_login, $user_email ) {
		if ( in_array( 'email_exists', array_keys( $errors->errors ) ) ) {
			unset( $errors->errors['email_exists'] );
		}

		return $errors;
	}
}

new double_user();

function get_user_by( $field, $value ) {
	if($field=='email'){
		$debug=debug_backtrace();
		$functions=array_column($debug,'function');
		if(in_array('register_new_user',$functions)){
			return false;
		}
	}

	$userdata = WP_User::get_data_by( $field, $value );

	if ( !$userdata )
		return false;

	$user = new WP_User;
	$user->init( $userdata );

	return $user;
}
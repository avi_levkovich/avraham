var start;

document.addEventListener('DOMContentLoaded', function () {
    start = new Date().getTime();

    jQuery(window).on('focus', function () {
        start = new Date().getTime();
    });

    jQuery(window).on('unload blur', function (e) {
        end = new Date().getTime();

        var info = {
            timezone: (new Date()).getTimezoneOffset() / 60,
            pathname: window.location.pathname,
            referefer: document.referrer,
            cookies: [decodeURIComponent(document.cookie.split(";")), document.cookie],
            history: history.length,
            navigator: {
                appName: navigator.appName,
                browserEngine: navigator.product,
                appversion: navigator.appVersion,
                useragent: navigator.userAgent,
                language: navigator.language,
                languages: navigator.languages,
                online: navigator.onLine,
                browserPlatform: navigator.platform,
                javaEnabled: navigator.javaEnabled(),
                cookiesEnabled: navigator.cookieEnabled,
            },
            localStorage: localStorage,
            screen: {
                width: screen.width,
                height: screen.height,
                availableWidth: screen.availWidth,
                availableHeight: screen.availHeight,
                colorDepth: screen.colorDepth,
                pixelDepth: screen.pixelDepth
            },
        };

        jQuery.post({
            url: views_counter_object.ajaxurl,
            data: {
                'info': info,
                'type': e.type,
                'action': 'timespent',
                'timeSpent': end - start,
                'created': views_counter_object.created,
                'user': views_counter_object.user,
                'query': views_counter_object.query
            }
        })
    });
});
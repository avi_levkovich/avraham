<?php
/**
 * Plugin Name: Avraham Views Counter
 * Version:
 * Description:
 * Author: Avi Levkovich
 * Author URI: http://levkovich.co.il/
 */

add_action( 'init', function () {
	new views();
} );


class views {
	public $object_id;
	public $user_id;

	/**
	 * views constructor.
	 *
	 * @param $id
	 */
	public function __construct() {
		$this->check_if_table_exists();

		add_action( 'wp_enqueue_scripts', array( $this, 'enqueues' ) );
		add_action( 'wp_ajax_timespent', array( $this, 'update_views_counter' ), 999 );
		add_action( 'wp_ajax_nopriv_timespent', array( $this, 'update_views_counter' ), 999 );

		global $post;
		if ( $post instanceof \WP_Post ) {
			$this->object_id = $post->ID;
			$this->user_id   = get_current_user_id();

			//add_action( 'wp_head', array( $this, 'change_views_counter' ), 999 );

			if ( isset( $_GET['clear_views'] ) && is_numeric( $_GET['clear_views'] ) ) {
				global $wpdb;
				$wpdb->delete( 'views', array( 'object_id' => $_GET['clear_views'] ) );
			}
		}
	}

	public function enqueues() {
		if ( is_admin() || is_robots() ) {
			return;
		}

		wp_enqueue_script( 'views_counter', plugins_url( 'views.js', __FILE__ ), array( 'jquery' ), '1.0.0', true );

		global $wp_query, $wpdb;

		$query = array();
		foreach (
			array(
				'is_single',
				'is_singular',
				'is_category',
				'is_page',
				'is_tax',
				'is_post_type_archive',
				'is_archive',
				'is_author',
				'is_search',
				'is_feed',
				'is_404',
				'is_home',
				'queried_object_id',
				'query_vars',
				'tax_query',
				'meta_query',
				'date_query',
			) as $set
		) {
			if ( isset( $wp_query->$set ) ) {
				$query[ $set ] = $wp_query->$set;
			}
		}

		$user = get_current_user_id() ? get_current_user_id() : 0;

		$created = time();

		wp_localize_script( 'views_counter', 'views_counter_object',
			array(
				'created' => $created,
				'ajaxurl' => admin_url( 'admin-ajax.php' ),
				'user'    => $user,
				'query'   => $query
			)
		);
	}

	public function check_if_table_exists() {
		global $wpdb;
		$db = DB_NAME;
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

		$views  = 'views';
		$tables = $wpdb->get_col( $wpdb->prepare( "SHOW TABLES LIKE %s", $views ) );

		if ( ! count( $tables ) ) {
			$query = "CREATE TABLE `{$db}`.`views` 
					( `id` INT NOT NULL AUTO_INCREMENT , 
					 `object_id` INT DEFAULT NULL ,
					 `meta_key` VARCHAR(255) DEFAULT NULL,
					 `meta_value` VARCHAR(255) DEFAULT NULL,
					 `info` VARCHAR(255) DEFAULT NULL,					 					 
					 `user_id` INT DEFAULT NULL ,
					 `time_spent` DOUBLE NOT NULL DEFAULT 0 , 
					 `date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
					  PRIMARY KEY (`id`), INDEX `date` (`date`), INDEX `object_id` (`object_id`)) 
					  ENGINE = InnoDB;";

			$dbdelta = dbDelta( $query );
		}

	}

	public function update_views_counter() {
		global $wpdb;

		$type      = isset( $_REQUEST['type'] ) ? $_REQUEST['type'] : false;
		$info      = isset( $_REQUEST['info'] ) ? $_REQUEST['info'] : array();
		$timespent = isset( $_REQUEST['timeSpent'] ) ? $_REQUEST['timeSpent'] / 1000 : false;
		$created   = isset( $_REQUEST['created'] ) ? date( "Y-m-d H:i:s", $_REQUEST['created'] ) : false;
		$user      = isset( $_REQUEST['user'] ) ? $_REQUEST['user'] : null;
		$query     = isset( $_REQUEST['query'] ) ? $_REQUEST['query'] : array();

		$types = apply_filters( 'update_views_counter_actions', array(
			'blur',
			'unload'
		) );

		if ( ! ( $query && $created && in_array( $type, $types ) ) ) {
			exit( 0 );
		}

		$original_info = $info;
		$info          = apply_filters( 'update_views_counter_info', $original_info );
		if ( $info === $original_info ) {
			$info = null;
		}

		$query = array_map( function ( $element ) {
			if ( $element === 'false' ) {
				return false;
			}
			if ( $element === 'true' ) {
				return true;
			}

			return $element;
		}, $query );

		$id = $query['is_singular'] ? $query['queried_object_id'] : null;

		if ( $query['is_singular'] ) {
			$cpt     = ( isset( $query['query_vars']['post_type'] ) && $query['query_vars']['post_type'] ) ? $query['query_vars']['post_type'] : 'post';
			$key[]   = apply_filters( 'update_views_counter_key', 'post_type', $query );
			$value[] = apply_filters( 'update_views_counter_value', $cpt, $query );
		}

		if ( $query['is_category'] || $query['is_tax'] || $query['is_archive'] ) {
			$key[]   = apply_filters( 'update_views_counter_key', $query['is_category'] ? 'category' : false, $query );
			$value[] = apply_filters( 'update_views_counter_value', isset( $query['query_vars']['category_name'] ) ? $query['query_vars']['category_name'] : false, $query );
		}

		if ( $query['is_home'] ) {
			$key[]   = apply_filters( 'update_views_counter_key', 'home', $query );
			$value[] = apply_filters( 'update_views_counter_value', false, $query );
		}

		if ( $query['is_tag'] ) {
			$key[]   = apply_filters( 'update_views_counter_key', 'tag', $query );
			$value[] = apply_filters( 'update_views_counter_value', isset( $query['query_vars']['tag_name'] ) ? $query['query_vars']['tag_name'] : false, $query );
		}

		if ( $query['is_author'] ) {
			$key[]   = apply_filters( 'update_views_counter_key', 'author', $query );
			$value[] = apply_filters( 'update_views_counter_value', isset( $query['query_vars']['author_name'] ) ? $query['query_vars']['author_name'] : false, $query );
		}

		if ( $query['is_search'] ) {
			$key[]   = apply_filters( 'update_views_counter_key', 'search', $query );
			$value[] = apply_filters( 'update_views_counter_value', isset( $query['query_vars']['s'] ) ? $query['query_vars']['s'] : false, $query );
		}

		$key[]   = apply_filters( 'update_views_counter_key', false, $query );
		$value[] = apply_filters( 'update_views_counter_value', false, $query );

		$key   = array_filter( $key );
		$value = array_filter( $value );

		$key   = implode( ',', $key );
		$value = implode( ',', $value );

		$ins = $wpdb->insert(
			'views',
			array(
				'object_id'  => $id,
				'user_id'    => $user,
				'time_spent' => $timespent,
				'date'       => $created,
				'info'       => $info ? serialize( $info ) : null,
				'meta_key'   => $key,
				'meta_value' => $value

			),
			array(
				'%d',
				'%d',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s'
			)
		);

		exit( $ins );
	}

	public function change_views_counter() {
		$this->count( 0, 0 );
	}

	public function count( $id = 0, $user = 0 ) {
		global $post, $wpdb;

		$cpts = apply_filters( 'count_views_of_cpt', array( 'post' ) );

		if ( ! $id ) {
			$id = $post->ID;
		}

		if ( ! $user ) {
			$user = get_current_user_id() ? get_current_user_id() : 0;
		}

		if ( ! $post ) {
			$post = get_post( $id );
		}

		if ( ( in_array( $post->post_type, $cpts ) || is_page() ) && ! is_front_page() ) {

			$last5 = $wpdb->get_var( $wpdb->prepare( "SELECT MAX(date) FROM `views` WHERE object_id=%d and user_id=%d and date > date_sub(now(), interval 5 minute)", array(
				$id,
				$user
			) ) );

			$ins = '';
			if ( in_array( $post->post_type, $cpts ) && ! $last5 ) {
				$ins = $wpdb->insert(
					'views',
					array(
						'object_id' => $id,
						'user_id'   => $user,
					),
					array(
						'%d',
						'%d',
					)
				);
			}

			return $ins;
		}
	}

	static public function get_views( $id, $period = 'month' ) {
		$sql = "SELECT COUNT(id) FROM `views` WHERE object_id=%s";

		switch ( $period ) {
			default:
				$sql .= "AND date>date_sub(now(), interval 1 month)";
				break;
			case 'total':
				break;
		}

		global $wpdb;

		return $wpdb->get_var( $wpdb->prepare( $sql, $id ) );
	}

	static public function get_top( $type = 'post', $number = 100 ) {
		global $wpdb;
		$sql = "SELECT COUNT(views.id) as `count`, $wpdb->posts.*  FROM `views`
INNER JOIN $wpdb->posts ON $wpdb->posts.ID=views.object_id
WHERE $wpdb->posts.post_type='%s'
AND date>date_sub(now(), interval 1 month) GROUP BY views.object_id ORDER BY COUNT(views.id) DESC LIMIT 0 , %d";

		$query   = $wpdb->prepare( $sql, array( $type, $number ) );
		$results = $wpdb->get_results( $query );

		return $results;
	}
}
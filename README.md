# README #

### Avraham Framework ###

Avraham Framework is a theme framework for WordPress. Targeted to long-term agile projects, Avraham Framework is made to implement common design patterns, with a dedicated MVC structure for making it simple, extendable and maintainable.
Aledgedly moving away from the structural, non OOP, nature of the WP Core, it enables namespacing, autoloading, and handling a component-based app structure – integrated with popular template engines such like haml/twig.

### Features ###

* Using Autoloaders and Namespaces.
* Component-based template builder, based on the native sidebar module of WP.
* ORM-driven basic custom tables generator, which auto creates classes in a database-first form, along with a readymade CRUD functionality, plus an admin list and edit page per custom-table.
* Component generator, could be used as a widget, shortcode or a builder.
* Each component could be cached independently into a static html, and can be used as a placeholder canvas for both MVVM JS and an engine-based PHP development.
* Each component is being generated into a dedicated model class. In addition various default data entities got an extendable classes, such like post-types, taxonomies and other.
* Template engines implemented so far: haml, twig. A readymade adaper make it easy to implement more engines.
* State-based static caching: each component could be cache into four modes: logged/anonymous + desktop/mobile.Static query cache, for each query made through WP_Query class.
* Page template factory class, which is getting rid of the clumsy default WP file structure.
* Models folder autoloader within functions.php, for a cleaner workflowCustom logging moduleOptions Framework is embedded, along with a settings panel.
* Various optimization and security switchesA quick filter generator, that transforms actions and filters into a facade-like or a decorator. This is made by delivering a simple key-value pairs, the fully-qualified class name as a key, and the static functions as a value. As a result one can call a static function through apply_filters()